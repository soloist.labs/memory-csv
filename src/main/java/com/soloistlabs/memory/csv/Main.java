package com.soloistlabs.memory.csv;

import com.soloistlabs.memory.csv.tsv.TSVReaderWriter;
import com.soloistlabs.memory.csv.tsv.TSVReaderWriterImpl;
import com.soloistlabs.memory.csv.tsv.TSVStreamer;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/*
    This is the application entry point and it runs 3 different implementations of a tab-separated-values file
    reader/writer.

     - The first implementation is called CSVReaderWriter and it's the one provided by Memory. It doesn't work very
       well :) My code review for it is in CSVReaderWriter.java

     - The second one is called TSVReaderWriter. It consists of an interface and a concrete implementation. It's
       compatible with the public interface of CSVReaderWriter, but it works better. It is also unit-tested
       in TSVReaderWriterImplTest.java

     - The third one is called TSVStreamer and it is a more "modern" and much simpler implementation based on Streams.
       It has a much simpler interface and more elegant implementation. This is also unit-tested in TSVStreamerTest.java
 */
public class Main {

    private static final String TEST_FILE = "test.txt";
    private static final String BIG_FILE = "nasa_19950801.tsv";

    public static void main(String[] args) {

        System.out.println("==== LEGACY CSV READER/WRITER ====");
        legacyCSVReaderWriterExample();

        System.out.println("\n\n==== BETTER TSV READER/WRITER ====");
        TSVReaderWriterExample();

        System.out.println("\n\n==== TSV STREAMER ====");
        TSVStreamerExample();

        // Uncomment this to test the TSVStreamer for reading a big TSV file
        // System.out.println("\n\n==== STREAM FROM A BIG FILE ====");
        // TSVStreamerReadBigFileExample();

    }

    private static void legacyCSVReaderWriterExample() {
        // Usage example of the legacy class CSVReaderWriter
        final CSVReaderWriter legacyCSVReaderWriter = new CSVReaderWriter();
        try {
            // These values are not written correctly as they are missing the newline
            legacyCSVReaderWriter.open(TEST_FILE, CSVReaderWriter.Mode.Write);
            legacyCSVReaderWriter.write("test1", "test2");
            legacyCSVReaderWriter.write("testA", "testB");
            legacyCSVReaderWriter.close();

            // These values are not read correctly
            legacyCSVReaderWriter.open(TEST_FILE, CSVReaderWriter.Mode.Read);
            String value1 = "";
            String value2 = "";
            legacyCSVReaderWriter.read(value1, value2);
            System.out.println(String.format("CSVReaderWriter values: \"%s\"  \"%s\"", value1, value2));
            legacyCSVReaderWriter.close();

            // These are read correctly (but were not written correctly in the first place)
            legacyCSVReaderWriter.open(TEST_FILE, CSVReaderWriter.Mode.Read);
            String[] buffer = new String[2];
            legacyCSVReaderWriter.read(buffer);
            System.out.println(String.format("CSVReaderWriter values: \"%s\"  \"%s\"", buffer[0], buffer[1]));
            legacyCSVReaderWriter.close();

            Files.deleteIfExists(Paths.get(TEST_FILE));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void TSVReaderWriterExample() {
        // Usage example of the better class TSVReaderWriter, which keeps API compatibility with the
        // legacy CSVReaderWriter (CSVReaderWriter could implement TSVReaderWriter)
        final TSVReaderWriter tsvReaderWriter = new TSVReaderWriterImpl();
        try {
            tsvReaderWriter.openInWriteMode(TEST_FILE);
            tsvReaderWriter.write("test1", "test2");
            tsvReaderWriter.write("testA", "testB");
            tsvReaderWriter.write("testC", "testD");
            tsvReaderWriter.close();

            tsvReaderWriter.openInReadMode(TEST_FILE);
            String[] buffer = new String[2];
            while (tsvReaderWriter.read(buffer)) {
                System.out.println(String.format("TSVReaderWriter values: \"%s\"  \"%s\"", buffer[0], buffer[1]));
            }
            tsvReaderWriter.close();
            Files.deleteIfExists(Paths.get(TEST_FILE));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void TSVStreamerExample() {
        // Usage example of the class TSVStreamer, which uses a more modern, stream-based API
        final TSVStreamer tsvStreamer = new TSVStreamer();
        try {

            final Stream<Stream<String>> values = Stream.of(
                    Stream.of("test1", "test2"),
                    Stream.of("testA", "testB", "", "testC"),
                    Stream.of("hello"));

            tsvStreamer.writeValues(values, TEST_FILE);

            tsvStreamer.readValues(TEST_FILE)
                    .forEach(stream -> {
                        System.out.print("TSVStreamer values:");
                        stream.forEach(value -> System.out.print(String.format(" \"%s\" ", value)));
                        System.out.println();
                    });

            Files.deleteIfExists(Paths.get(TEST_FILE));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void TSVStreamerReadBigFileExample() {
        final TSVStreamer tsvStreamer = new TSVStreamer();
        try {
            tsvStreamer.readValues(BIG_FILE)
                    .forEach(stream -> {
                        stream.forEach(value -> System.out.print(String.format(" %s ", value)));
                        System.out.println();
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
