package com.soloistlabs.memory.csv.tsv;

import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TSVStreamer {

    private static final String SEPARATOR = "\t";
    private static final String NEW_LINE = "\n";

    /**
     * Reads values from a TSV file as a stream
     *
     * @param fileName The file to read values from. Cannot be null
     * @return The stream of values
     * @throws Exception If the parameter is null of in case of an I/O error
     */
    @NotNull
    public Stream<Stream<String>> readValues(@NotNull final String fileName) throws Exception {
        if (fileName == null) {
            throw new IllegalArgumentException("Parameter fileName cannot be null");
        }

        final Stream<String> lines = Files.lines(Paths.get(fileName));
        final Function<String, Stream<String>> splitToValues = line -> Pattern.compile(SEPARATOR).splitAsStream(line);
        return lines.map(splitToValues);
    }

    /**
     * Writes a stream of values to a TSV file
     *
     * @param values The stream of values to write. Values should not contain the TAB \t character or the new line
     *               character
     * @param fileName The file to write to
     * @throws Exception If the parameters are null of in case of an I/O error
     */
    public void writeValues(@NotNull final Stream<Stream<String>> values, @NotNull final String fileName) throws Exception {
        if (values == null || fileName == null) {
            throw new IllegalArgumentException("Parameters cannot be null");
        }

        final Stream<String> lines = values.map(valuesStream -> valuesStream
                .peek(value -> {
                    if (value.contains(SEPARATOR) || value.contains(NEW_LINE)) {
                        throw new IllegalArgumentException("Input value contains an illegal character like TAB or a new line");
                    }
                })
                .collect(Collectors.joining(SEPARATOR)));
        Files.write(Paths.get(fileName), (Iterable<String>) lines::iterator);
    }

}
