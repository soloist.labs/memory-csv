package com.soloistlabs.memory.csv.tsv;

import com.soloistlabs.memory.csv.CSVReaderWriter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TSVReaderWriterImpl implements TSVReaderWriter {

    private static final String SEPARATOR = "\t";
    private static final String NEW_LINE = "\n";

    @Nullable
    private BufferedReader mReader;

    @Nullable
    private BufferedWriter mWriter;

    @Override
    public void openInReadMode(@NotNull final String fileName) throws Exception {
        mReader = Files.newBufferedReader(getPathForOpening(fileName));
    }

    @Override
    public void openInWriteMode(@NotNull final String fileName) throws Exception {
        mWriter = Files.newBufferedWriter(getPathForOpening(fileName));
    }

    @Deprecated
    @Override
    public void open(@NotNull final String fileName, @NotNull final CSVReaderWriter.Mode mode) throws Exception {
        if (mode == null) {
            throw new IllegalArgumentException("Parameters cannot be null");
        }

        switch (mode) {
            case Read:
                openInReadMode(fileName);
                break;
            case Write:
                openInWriteMode(fileName);
                break;
            default:
                throw new IllegalArgumentException("Only Read or Write are valid modes");
        }
    }

    @Override
    public void close() throws IOException {
        if (isInReadMode()) {
            mReader.close();
            mReader = null;
        } else if (isInWriteMode()) {
            mWriter.close();
            mWriter = null;
        }
    }

    @Override
    public void write(@NotNull final String... columns) throws Exception {
        if (!isInWriteMode()) {
            throw new IllegalStateException("Not in Write mode");
        }

        if (columns == null) {
            throw new IllegalArgumentException("Parameter cannot be null");
        }

        if (columns.length == 0) {
            throw new IllegalArgumentException("No value to write");
        }

        final String record = buildRecord(columns);
        if (!record.isEmpty()) {
            mWriter.write(record);
            mWriter.newLine();
        }
    }

    @Override
    public boolean read(@NotNull final String[] columns) throws Exception {
        if (!isInReadMode()) {
            throw new IllegalStateException("Not in Read mode");
        }

        if (columns == null) {
            throw new IllegalArgumentException("Parameter cannot be null");
        }

        if (columns.length == 0) {
            throw new IllegalArgumentException("Reading buffer should be able to contain at least 1 value");
        }

        final String record = mReader.readLine();
        if (record == null) {
            return false;
        } else {
            parseValues(record, columns);
            return true;
        }
    }

    @Deprecated
    @Override
    public boolean read(@NotNull final String column1, @NotNull final String column2) throws IOException {
        return false;
    }

    private boolean isOpen() {
        return isInReadMode() || isInWriteMode();
    }

    private boolean isInWriteMode() {
        return mWriter != null;
    }

    private boolean isInReadMode() {
        return mReader != null;
    }

    private Path getPathForOpening(@NotNull final String fileName) throws Exception {
        if (isOpen()) {
            throw new IllegalStateException("File already open");
        }
        if (fileName == null) {
            throw new IllegalArgumentException("Parameters cannot be null");
        }
        return Paths.get(fileName);
    }

    @NotNull
    private String buildRecord(@NotNull final String... columns) throws IllegalArgumentException {
        final StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < columns.length; i++) {
            final String value = columns[i] == null ? "" : columns[i];
            if (value.contains(SEPARATOR) || value.contains(NEW_LINE)) {
                throw new IllegalArgumentException("Input value contains an illegal character like TAB or a new line");
                //TODO check for other corner cases?
            } else {
                stringBuilder.append(value);
                if (i < columns.length - 1) {
                    stringBuilder.append(SEPARATOR);
                }
            }
        }
        return stringBuilder.toString();
    }

    private void parseValues(@NotNull final String record, @NotNull final String[] columns) {
        final String[] values = record.split(SEPARATOR, -1);
        for (int i = 0; i < columns.length; i++) {
            final String value = i < values.length ? values[i] : "";
            columns[i] = value;
        }
    }
}
