package com.soloistlabs.memory.csv.tsv;

import com.soloistlabs.memory.csv.CSVReaderWriter;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

/**
 * Reader/Writer of Tab-Separated-Values files
 */
public interface TSVReaderWriter {

    /**
     * Opens the specified TSV file for reading
     *
     * @param fileName The TSV file to read from. Should not be null.
     * @throws IllegalArgumentException If the fileName parameter is null
     * @throws IllegalStateException    if this method is called more than once without calling {@link #close()} first
     * @throws IOException              If there is an I/O error while opening the file
     */
    void openInReadMode(@NotNull final String fileName) throws Exception;

    /**
     * Opens the specified TSV file for writing
     *
     * @param fileName The TSV file to write to. Should not be null.
     * @throws IllegalArgumentException If the fileName parameter is null
     * @throws IllegalStateException    if this method is called more than once without calling {@link #close()} first
     * @throws IOException              If there is an I/O error while opening the file
     */
    void openInWriteMode(@NotNull final String fileName) throws Exception;

    /**
     * Opens the specified TSV file for reading or writing, depending on the specified Mode
     *
     * @param fileName The TSV file to read from or write to. Should not be null.
     * @param mode     Use {@link CSVReaderWriter.Mode#Read} to read values from
     *                 the specified file or {@link CSVReaderWriter.Mode#Write} to
     *                 write values to the file. Should not be null.
     * @throws IllegalArgumentException If either parameter is null or if the specified mode in unknown
     * @throws IllegalStateException    if this method is called more than once without calling {@link #close()} first
     * @throws IOException              If there is an I/O error while opening the file
     * @deprecated This method has been deprecated. Use {@link #openInReadMode(String)} or {@link #openInWriteMode(String)} instead.
     */
    @Deprecated
    void open(@NotNull final String fileName, @NotNull final CSVReaderWriter.Mode mode) throws Exception;

    /**
     * Closes the previously opened file and frees all underlying resources.
     * <p>
     * If the file was not open, then this call is ignored.
     *
     * @throws IOException if an I/O error occurred while closing the file
     */
    void close() throws IOException;

    /**
     * Writes a new record (line) containing the specified values to the currently opened file
     *
     * @param columns The values to be written.
     *                There must be at least one value. Values should not contain the TAB \t character or the new line
     *                character. A null value is treated as an empty column.
     * @throws IllegalArgumentException If the input parameter does not contain any value
     * @throws IllegalStateException    If the file is not currently open in {@link CSVReaderWriter.Mode#Write} mode
     * @throws IOException              If an I/O error occurred while writing the file
     */
    void write(@NotNull final String... columns) throws Exception;

    /**
     * Read values from the next record (line) containing at least 1 value of the currently opened file.
     *
     * @param columns The values read will be stored in this provided array. If there are more values available than
     *                then amount this array can contain, only the first values will be retrieved. Should not be null.
     * @return false if there are no more values available in the file, true otherwise.
     * @throws IllegalArgumentException If the input parameter is not big enough to contain at least 1 value
     * @throws IllegalStateException    If the file is not currently open in {@link CSVReaderWriter.Mode#Read} mode
     * @throws IOException              If an I/O error occurred while reading the file
     */
    boolean read(@NotNull final String[] columns) throws Exception;

    /**
     * This method was meant to read 2 values from the next line containing at least a value of the currently
     * opened file. Unfortunately it does not work, and always returns false.
     *
     * @param column1 This value will be ignored.
     * @param column2 This value will be ignored.
     * @return Always false.
     * @throws IOException Never
     * @deprecated This method doesn't work and is kept only for backward compatibility. It doesn't perform any
     * operation and always returns false. Use {@link #read(String[])} instead.
     */
    @Deprecated
    boolean read(@NotNull final String column1, @NotNull final String column2) throws IOException;

}
