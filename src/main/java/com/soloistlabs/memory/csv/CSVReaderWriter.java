package com.soloistlabs.memory.csv;

/*
A developer was tasked with writing a reusable implementation for a mass mailing application to read and write text files that hold tab separated data. He proceeded and as a result produced the CSVReaderWriter class.

His implementation, although it works and meets the needs of the application, is of very low quality.

Your task:
     - Identify and annotate the shortcomings in the current implementation as if you were doing a code review, using comments in the CSVReaderWriter.java file.
     - Refactor the CSVReaderWriter implementation into clean, elegant, rock-solid & well performing code, without over-engineering.
     - Where you make trade offs, comment & explain.
     - Assume this code is in production and backwards compatibility must be maintained. Therefore if you decide to change the public interface,
       please deprecate the existing methods. Feel free to evolve the code in other ways though.
     - You can add modules and jars as you see fit as long as everything will build on our systems without manual intervention.
*/


/*
    ==== CODE REVIEW by Alessandro Bizzarri ====

    In my comments I'm going to assume that:

       * We actually want to write our own TSV writer/reader implementation and not use a 3rd party library that
         may already do the job and handle corner cases better

       *  This class could be part of a public library, so the public interface is particularly important

   General Comments:

   - The class lacks some form of API documentation. Preferably the public method should be
     annotated with the javadoc notation.

   - The class name is a bit misleading. CSV means comma-separated-values while the class is parsing
     tab-separated-values files.
     These formats are not very standard and it's common that files that contain values separated with other separators
     than comma (tabs, spaces, ...) are still given a .csv extension, so it would just be better to be extra clear.

   - The public interface of this class could be split to an actual separate interface. Even if we will always have a
     single concrete implementation of a CSVReaderWriter, this could help distinguish between what's important for the
     user of the class and what are details of this concrete implementation.

   - The public API is also not very "user-friendly" in the sense that forces who is using it to call or not call
     certain methods depending on the current state of this class, which is not retrievable externally.
     For example we can call read or write regardless of the Mode we opened the file with. We can call open/close
     multiple times. We can call open after a read/write, hence replacing the Readers/Writers without having closed
     them, etc.

   - I would prefer if the reading/writing from/to files "functionality" was separated from the actual TSV parsing.
     This would align better with the single responsibility principle and allow for better testability

   - The class is lacking tests

   - Since the class is written in Java, which doesn't enforce null safety (as for example Kotlin) it would be nice to
     annotate parameters and return values in public method with nullability annotations (like @NonNull). This helps
     both the user of the class to understand better and also is used by several static checking tools to reveal
     potential bugs.
 */

import java.io.*;

public class CSVReaderWriter {
    private BufferedReader _bufferedReader = null;
    private BufferedWriter _bufferedWriter = null;

    public enum Mode {
        Read (1), Write(2);

        private int _mode;
        Mode(int mode) {
            this._mode = mode;
        }
        public int getMode() {
            return _mode;
        }
    }

    // - Parameters should be declared as final
    // - Parameters should be checked for nullability
    // - It would be better to throw a specific exception i.e. IllegalArgumentException
    public void open(String fileName, Mode mode) throws Exception {
        // Use a Switch statement
        if (mode == Mode.Read)
        {
            _bufferedReader = new BufferedReader(new FileReader(fileName));
        }
        else if (mode == Mode.Write)
        {
            FileWriter fileWriter = new FileWriter(fileName);
            _bufferedWriter = new BufferedWriter(fileWriter);
        }
        else
        {
            // The whole point of using an Enum is that you exactly define what values are allowed.
            // Mode can be only be Read, Write or null. Maybe null checking would be enough, and in that case
            // you should handle also fileName nullability
            throw new Exception("Unknown file mode for " + fileName);
        }
    }

    // - Parameters should be declared as final
    // - The contents of columns could be checked to make sure they don't contain TAB character. In any case it would
    //   be nice to explicitly mention it.
    public void write(String... columns) throws IOException {
        String outPut = ""; // Use StringBuilder

        for (int i = 0; i < columns.length; i++)
        {
            outPut += columns[i]; // Concatenating strings in a loop like this is not efficient. Use StringBuilder or equivalent instead
            if ((columns.length - 1) != i)
            {
                outPut += "\t"; // Create a static final constant for the separator
            }
        }

        writeLine(outPut); // Do we want to write an empty line if the are no values passed in (if columns is size 0)?
    }

    // - Parameter should be declared as final
    public boolean read(String[] columns) throws IOException {
        // Wny are we implying that there are only 2 columns?
        final int FIRST_COLUMN = 0;
        final int SECOND_COLUMN = 1;

        String line;
        String[] splitLine;

        // The separator should be a static constant
        // If we are following some standard, like IANA https://www.iana.org/assignments/media-types/text/tab-separated-values
        // it may be worth communicating this in the API. This standard for example states clearly that the tab character
        // cannot be part of the value itself.
        String separator = "\t";

        line = readLine();
        splitLine = line.split(separator); // line may be null

        if (splitLine.length == 0)
        {
            columns[0] = null;
            columns[1] = null;

            return false; // Do we plan to return false both when a line is empty and we reached the EOF?
            // If somebody is iterating on read() to understand when no more values are available (we are at the end of
            // the file, that wouldn't work and we would stop whenever we find an empty line instead
        }
        else
        {
            // What if there is only one column?
            // What if there are more than 2 columns?
            columns[0] = splitLine[FIRST_COLUMN];
            columns[1] = splitLine[SECOND_COLUMN]; // This may cause out of bounds

            return true;
        }
    }

    /*
      Unfortunately this method is useless. It's not possible to pass Strings by "reference" this way.
      We cannot replace the values in the passed-in column1 and column2 and make them "survive" outside the scope of
      this method. This means you haven't even tried to use this method!

      Apart for this, there is also code duplication between this method and the one above
     */
    public boolean read(String column1, String column2) throws IOException {
        final int FIRST_COLUMN = 0;
        final int SECOND_COLUMN = 1;

        String line;
        String[] splitLine;

        String separator = "\t";

        line = readLine();

        if (line == null) // here you checked for null but you didn't above
        {
            column1 = null;
            column2 = null;

            return false;
        }

        splitLine = line.split(separator);

        if (splitLine.length == 0)
        {
            column1 = null;
            column2 = null;

            return false;
        }
        else
        {
            column1 = splitLine[FIRST_COLUMN];
            column2 = splitLine[SECOND_COLUMN]; // As above, there may be only 1 value in splitLine.

            return true;
        }
    }

    // - Parameter should be  final
    private void writeLine(String line) throws IOException {
        // _bufferedWriter could be null! This private method
        // is called by a public one that doesn't check for nullability
        _bufferedWriter.write(line);
        // _bufferedWriter.newLine() should also be called, otherwise everything will be written in the same line :(
    }

    private String readLine() throws IOException {
        // _bufferedReader could be null. This private method
        // is called by a public one that doesn't check for nullability
        return _bufferedReader.readLine();
    }

    public void close() throws IOException {
        if (_bufferedWriter != null)
        {
            _bufferedWriter.close();
        }

        if (_bufferedWriter != null)
        {
            _bufferedWriter.close();
        }
    }
}
