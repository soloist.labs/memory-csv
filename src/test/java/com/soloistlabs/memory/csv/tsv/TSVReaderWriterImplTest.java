package com.soloistlabs.memory.csv.tsv;

import com.soloistlabs.memory.csv.CSVReaderWriter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class TSVReaderWriterImplTest {

    private static final String FILE_NOT_EXISTING = "not_existing.tsv";
    private static final String FILE_EXISTING = "existing.tsv";

    private TSVReaderWriter mUnderTest;

    @Before
    public void setUp() throws IOException {
        Files.deleteIfExists(Paths.get(FILE_NOT_EXISTING));
        Files.deleteIfExists(Paths.get(FILE_EXISTING));
        Files.createFile(Paths.get(FILE_EXISTING));
        mUnderTest = new TSVReaderWriterImpl();
    }

    @After
    public void tearDown() throws IOException {
        Files.deleteIfExists(Paths.get(FILE_NOT_EXISTING));
        Files.deleteIfExists(Paths.get(FILE_EXISTING));
    }

    @Test(expected = IOException.class)
    public void fileToReadShouldExist() throws Exception {
        mUnderTest.open(FILE_NOT_EXISTING, CSVReaderWriter.Mode.Read);
    }

    @Test(expected = IllegalStateException.class)
    public void fileShouldNotBeOpenedTwice() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Write);
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Read);
    }

    @Test
    public void fileCanBeClosedWithoutBeingOpen() throws IOException {
        mUnderTest.close();
    }

    @Test(expected = IllegalStateException.class)
    public void valuesShouldNotBeReadIfFileWasNotOpened() throws Exception {
        mUnderTest.read(new String[1]);
    }

    @Test(expected = IllegalStateException.class)
    public void valuesShouldNotBeReadIfFileWasNotOpenedForReading() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Write);
        mUnderTest.read(new String[1]);
    }

    @Test(expected = IllegalStateException.class)
    public void valuesShouldNotBeReadIfFileWasClosed() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Read);
        mUnderTest.close();
        mUnderTest.read(new String[1]);
    }

    @Test(expected = IllegalStateException.class)
    public void valuesShouldNotBeWrittenIfFileWasNotOpened() throws Exception {
        mUnderTest.write("hello", "hello2");
    }

    @Test(expected = IllegalStateException.class)
    public void valuesShouldNotBeWrittenIfFileWasNotOpenedForWriting() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Read);
        mUnderTest.write("hello", "hello2");
    }

    @Test(expected = IllegalStateException.class)
    public void valuesShouldNotBeWrittenIfFileWasClosed() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Write);
        mUnderTest.close();
        mUnderTest.write("hello", "hello2");
    }

    @Test(expected = IllegalArgumentException.class)
    public void fileNameShouldNotBeNull() throws Exception {
        mUnderTest.open(null, CSVReaderWriter.Mode.Read);
    }

    @Test(expected = IllegalArgumentException.class)
    public void modeShouldNotBeNull() throws Exception {
        mUnderTest.open(FILE_EXISTING, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void bufferToReadIntoShouldNotBeNull() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Read);
        mUnderTest.read(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void atLeastOneValueShouldBeWritten() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Write);
        mUnderTest.write();
    }

    @Test(expected = IllegalArgumentException.class)
    public void valuesToBeWrittenShouldNotContainTabs() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Write);
        mUnderTest.write("hello", "world\t!!");
    }

    @Test(expected = IllegalArgumentException.class)
    public void valuesToBeWrittenShouldNotContainNewLine() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Write);
        mUnderTest.write("hello", "world\n!!");
    }

    @Test
    public void testObjectCanBeReused() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Write);
        mUnderTest.write("hello", "World!!");
        mUnderTest.close();
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Write);
        mUnderTest.write("hello2", "World2!!");
        mUnderTest.close();
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Read);
        mUnderTest.read(new String[1]);
        mUnderTest.close();
    }

    @Test
    public void testValuesWrittenAreReadCorrectly() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Write);
        mUnderTest.write("hello", "World!!");
        mUnderTest.write("pippo", "", "mario", null);
        mUnderTest.write("maow", "woof", "roar");
        mUnderTest.write(null, "", null);
        mUnderTest.close();

        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Read);
        String[] buffer = new String[2];
        mUnderTest.read(buffer);
        assertEquals("hello", buffer[0]);
        assertEquals("World!!", buffer[1]);

        buffer = new String[5];
        mUnderTest.read(buffer);
        assertEquals("pippo", buffer[0]);
        assertEquals("", buffer[1]);
        assertEquals("mario", buffer[2]);
        assertEquals("", buffer[3]); // This value should be explicitly filled with empty string
        assertEquals("", buffer[4]); // This value should not be filled, but it's initialized to empty string

        buffer = new String[2];
        mUnderTest.read(buffer);
        assertEquals("maow", buffer[0]);
        assertEquals("woof", buffer[1]);

        buffer = new String[3];
        mUnderTest.read(buffer);
        assertEquals("", buffer[0]);
        assertEquals("", buffer[1]);
        assertEquals("", buffer[2]);

        mUnderTest.close();
    }

    @Test
    public void readingValuesShouldReturnTrueIfAvailable() throws Exception {
        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Write);
        mUnderTest.write("hello", "World!!");
        mUnderTest.close();

        mUnderTest.open(FILE_EXISTING, CSVReaderWriter.Mode.Read);
        String[] buffer = new String[2];
        assertTrue(mUnderTest.read(buffer));
        assertFalse(mUnderTest.read(buffer));
        mUnderTest.close();
    }
}
