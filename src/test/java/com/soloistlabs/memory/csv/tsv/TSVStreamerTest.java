package com.soloistlabs.memory.csv.tsv;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TSVStreamerTest {

    private static final String FILE_NOT_EXISTING = "not_existing.tsv";
    private static final String FILE_EXISTING = "existing.tsv";

    private TSVStreamer mUnderTest;

    @Before
    public void setUp() throws IOException {
        Files.deleteIfExists(Paths.get(FILE_NOT_EXISTING));
        Files.deleteIfExists(Paths.get(FILE_EXISTING));
        Files.createFile(Paths.get(FILE_EXISTING));
        mUnderTest = new TSVStreamer();
    }

    @After
    public void tearDown() throws IOException {
        Files.deleteIfExists(Paths.get(FILE_NOT_EXISTING));
        Files.deleteIfExists(Paths.get(FILE_EXISTING));
    }

    @Test(expected = IOException.class)
    public void fileToReadShouldExist() throws Exception {
        mUnderTest.readValues(FILE_NOT_EXISTING);
    }

    @Test(expected = IllegalArgumentException.class)
    public void fileNameShouldNotBeNull() throws Exception {
        mUnderTest.readValues(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void valuesToBeWrittenShouldNotBeNull() throws Exception {
        mUnderTest.writeValues(null, FILE_EXISTING);
    }

    @Test(expected = IllegalArgumentException.class)
    public void valuesToBeWrittenShouldNotContainTabs() throws Exception {
        mUnderTest.writeValues(Stream.of(Stream.of("hello", "world\t!!")), FILE_EXISTING);
    }

    @Test(expected = IllegalArgumentException.class)
    public void valuesToBeWrittenShouldNotContainNewLine() throws Exception {
        mUnderTest.writeValues(Stream.of(Stream.of("hello", "world\n!!")), FILE_EXISTING);
    }

    @Test
    public void testValuesWrittenAreReadCorrectly() throws Exception {
        final Supplier<Stream<Stream<String>>> originalValues = () -> Stream.of(
                Stream.of("test1", "test2"),
                Stream.of("testA", "testB", "", "testC"),
                Stream.of("hello"));

        mUnderTest.writeValues(originalValues.get(), FILE_EXISTING);
        final Stream<Stream<String>> readValues = mUnderTest.readValues(FILE_EXISTING);

        final Iterator<String> originalValuesIt = originalValues.get().flatMap(Function.identity()).iterator();
        final Iterator<String> readValuesIt = readValues.flatMap(Function.identity()).iterator();

        while (originalValuesIt.hasNext() && readValuesIt.hasNext()) {
            assertEquals(originalValuesIt.next(), readValuesIt.next());
        }
        assertTrue(!originalValuesIt.hasNext() && !readValuesIt.hasNext());
    }
}
