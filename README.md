# TSV Reader / Writer

This is my test for Memory.ai

It contains 3 different implementations of a tab-separated-values file reader/writer.

1. The first implementation is called **CSVReaderWriter** and it's the one provided by Memory. It doesn't work very well :) My code review for it is in [CSVReaderWriter.java](src/main/java/com/soloistlabs/memory/csv/CSVReaderWriter.java)

2. The second one is called **TSVReaderWriter**. It consists of an interface and a concrete implementation. It's compatible with the public interface of CSVReaderWriter, but it works better. It is also unit-tested in *TSVReaderWriterImplTest.java*

3. The third one is called **TSVReaderWriter** and it is a more "modern" implementation based on Streams. It has a much simpler interface and more elegant implementation. This is also unit-tested in *TSVStreamerTest.java*

## How to use

The code in this repo includes an Intellij IDEA project and can be build with gradle

##### How to build from command line

    ./gradlew build

##### Hot to run it from command line

    ./gradlew run

##### How to run the unit tests from command line

    ./gradlew test



